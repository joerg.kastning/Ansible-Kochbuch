\chapter{Linux-Benutzerkonten mit Ansible verwalten}
Wie bereits in \parencite{Kastning2016} erwähnt, beschäftige ich mich aktuell mit der Evaluierung von Ansible.

In diesem Artikel dokumentiere ich ein Beispiel, wie das Modul \key{user} zur Administration von Linux-Benutzerkonten verwendet werden kann. Der Link zur offiziellen Dokumentation des \key{user}-Moduls befindet sich bei \parencite{RedHatb}.

\emph{Hinweis:} Die folgende Konfiguration ist nicht zur Nachahmung empfohlen. Es handelt sich dabei um meine ersten Schritte mit Ansible. Es ist daher wahrscheinlich, dass die Konfiguration noch Fehler enthält, nicht optimiert ist und ein großes Verbesserungspotenzial besitzt. Die Nachahmung erfolgt ausdrücklich auf eigene Gefahr. Über Hinweise, wie man es besser machen kann, freue ich mich jederzeit.
\section{Anforderungen}
\begin{itemize}
	\item Es soll ein lokales Benutzerkonto angelegt werden
	\item Der Benutzer soll Mitglied der Gruppe "wheel" werden
	\item Es soll ein initiales Passwort für das Benutzerkonto gesetzt werden
	\item Ein öffentlicher SSH-Schlüssel soll in die Datei \key{authorized\_keys} des zu erstellenden Benutzers eingetragen werden
	\item Konfiguration des SSH-Daemons
\end{itemize}

\section{Vorbereitung}
Abgeleitet aus den \glqq Best Practices\glqq\ \parencite{RedHatc} verwende ich auf meiner Spielwiese folgende Verzeichnisstruktur:
\lstbash
\begin{lstlisting}[caption={Verzeichnisstruktur},label=lst:Verzeichnisstruktur]
ansible/
|-- autoupdate.txt            # ignore
|-- autoupdate.yml            # ignore
|-- check_reboot.             # playbook which sets the repos
|-- group_vars
|   `-- e-stage               # variables for group e-stage
|-- hosts                     # inventory file
|-- host_vars                 # for system specific variables
|-- roles
|   |-- common                # this hierarchy represents a "role"
|   |   |-- defaults          
|   |   |-- files
|   |   |-- handlers
|   |   |   `-- main.yml      # handlers file
|   |   |-- meta
|   |   |-- tasks
|   |   |   `-- main.yml      # tasks file can include smaller files if warranted
|   |   `-- vars
|   `-- e-stage
|       |-- defaults
|       |-- files
|       |-- handlers
|       |-- meta
|       |-- templates
|       `-- vars
|-- create_johnd.yml          # playbook which creates user johnd
|-- site.yml                  # master playbook file
`-- staging                   # inventory file for staging environment
\begin{verbatim}
ansible/
|-- autoupdate.txt            # ignore
|-- autoupdate.yml            # ignore
|-- check_reboot.             # playbook which sets the repos
|-- group_vars
|   `-- e-stage               # variables for group e-stage
|-- hosts                     # inventory file
|-- host_vars                 # for system specific variables
|-- roles
|   |-- common                # this hierarchy represents a "role"
|   |   |-- defaults          
|   |   |-- files
|   |   |-- handlers
|   |   |   `-- main.yml      # handlers file
|   |   |-- meta
|   |   |-- tasks
|   |   |   `-- main.yml      # tasks file can include smaller files if warranted
|   |   `-- vars
|   `-- e-stage
|       |-- defaults
|       |-- files
|       |-- handlers
|       |-- meta
|       |-- templates
|       `-- vars
|-- create_johnd.yml          # playbook which creates user johnd
|-- site.yml                  # master playbook file
`-- staging                   # inventory file for staging environment

17 directories, 11 files
\end{lstlisting}

Um das Passwort für den neuen Benutzer erstellen zu können, muss zuerst der passende Passwort-Hash generiert werden \parencite{RedHatd}:
\begin{lstlisting}[caption={Generierung eines Passowort-Hashs},label=lst:passwordhash]
python -c "from passlib.hash import sha512_crypt; import getpass; print sha512_crypt.encrypt(getpass.getpass())"
\end{lstlisting}
Der generierte Hash wird auf der Standardausgabe ausgegeben. Dieser wird für das Playbook benötigt.

\section{Playbook und Tasks}
Das Playbook ist sehr kurz gehalten und gibt im Wesentlichen nur an, welche Hosts auf welche Rolle gemapped werden sollen.

\begin{lstlisting}[caption={Ansible Playbook},label=lst:playbook]
# create_johnd.yml
---
- hosts: all
  roles:
    - common
\end{lstlisting}

Bei Verwendung dieses Playbooks werden die Tasks aus der Datei \key{roles/common/tasks/main.yml} ausgeführt, welche wie folgt aufgebaut wurde:
\begin{lstlisting}[caption={Datei: roles/common/tasks/main.yml},label=lst:tasks]
---
# Configure sshd_config on target system
- name: Enable Public-Key-Authentication
  lineinfile:
    dest: /etc/ssh/sshd_config
    regexp: "^PubkeyAuthentication"
    line: "PubkeyAuthentication yes"
    state: present
  notify:
    - reload sshd

- name: Set AuthorizedKeyFile
  lineinfile:
    dest: /etc/ssh/sshd_config
    regexp: "^AuthorizedKeysFile"
    line: "AuthorizedKeysFile      .ssh/authorized_keys"
    state: present
  notify:
    - reload sshd

- name: Disable PasswordAuthentication
  lineinfile:
    dest: /etc/ssh/sshd_config
    regexp: "^PasswordAuthentication"
    line: "PasswordAuthentication no"
    state: present
  notify:
    - reload sshd

# Add user johnd with specific uid in group 'wheel'
- user: name=johnd comment="John Doe" uid=100 groups=wheel password="PASSWORDHASH" shell=/bin/bash append=yes state=present
  notify:
    - deploy ssh-pub-keys
\end{lstlisting}

Die in \vref{lst:tasks} spezifizierten Tasks setzen die folgenden Aufgaben um:
\begin{itemize}
	\item Aktivierung der Public-Key-Authentication
	\item Spezifikation des Standard-AuthorizedKeysFile
	\item Deaktivierung der Password-Authentication
\end{itemize}

Was in \vref{lst:tasks} noch nicht zu sehen ist, ist die Verteilung eines öffentlichen SSH-Schlüssels. Dies wird von dem Handler \key{deploy ssh-pub-keys} übernommen.

Der Handler ist in der Datei \key{roles/common/handlers/main.yml} definiert. Es handelt sich dabei um einen Task, welcher nur ausgeführt wird, wenn der Benutzer erfolgreich erstellt wurde.

\section{Fazit}
Soweit zu meinen ersten Gehversuchen mit Ansible Playbooks. Meine Anforderungen können mit dem hier dokumentierten Playbook erfüllt werden.

In dem hier beschriebenen Beispiel habe ich angenommen, dass es sich bei dem zu erstellenden Benutzer um das erste lokale Benutzerkonto neben root handelt. Dieses soll über die Berechtigung verfügen, Kommandos mit \key{sudo} auszuführen und primär für die Arbeit auf dem System verwendet werden.

Die Verteilung des SSH-Schlüssels über einen Handler anzustoßen erscheint mir jedoch nicht optimal. Der Handler wird nur dann ausgeführt, wenn durch das \key{user}-Modul Änderungen am Zielsystem vorgenommen wurden. Weitere SSH-Schlüssel bzw. Änderungen an bereits vorhandenen SSH-Schlüsseln lassen sich auf diesem Weg nicht vornehmen.

Daher erscheint es mir sinnvoll, die Verteilung von SSH-Schlüsseln über das Modul \key{authorized\_key} \parencite{RedHate} in ein eigenes Playbook auszulagern.