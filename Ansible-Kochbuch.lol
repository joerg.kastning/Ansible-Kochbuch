\defcounter {refsection}{0}\relax 
\addvspace {10\p@ }
\defcounter {refsection}{0}\relax 
\addvspace {10\p@ }
\defcounter {refsection}{0}\relax 
\addvspace {10\p@ }
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {3.1}Verzeichnisstruktur}{3}{lstlisting.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {3.2}Generierung eines Passowort-Hashs}{4}{lstlisting.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {3.3}Ansible Playbook}{5}{lstlisting.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {3.4}Datei: roles/common/tasks/main.yml}{5}{lstlisting.3.4}
\defcounter {refsection}{0}\relax 
\addvspace {10\p@ }
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4.1}Genutzte Verzeichnisstruktur}{7}{lstlisting.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4.2}Ansible-Playbook: set\_repos.yml}{8}{lstlisting.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4.3}Tasks zum Hinzuf\IeC {\"u}gen von Repositories}{8}{lstlisting.4.3}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4.4}Datei: group\_vars/e-stage}{8}{lstlisting.4.4}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4.5}Datei: roles/e-stage/tasks/main.yml}{8}{lstlisting.4.5}
\defcounter {refsection}{0}\relax 
\addvspace {10\p@ }
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {5.1}Zur Umsetzung ben\IeC {\"o}tigte Dateien}{10}{lstlisting.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {5.2}Crontab des Benutzers root}{11}{lstlisting.5.2}
\defcounter {refsection}{0}\relax 
\addvspace {10\p@ }
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {6.1}Playbook zum Filtern der RHEL-Systeme}{12}{lstlisting.6.1}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {6.2}Datei: roles/patch\_rhel/vars/main.yml}{12}{lstlisting.6.2}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {6.3}Datei: roles/patch\_rhel/tasks/main.yml}{13}{lstlisting.6.3}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {6.4}Skript zur Bestimmung der Stichtage}{13}{lstlisting.6.4}
\defcounter {refsection}{0}\relax 
\addvspace {10\p@ }
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {7.1}Aufbau eines Ansible-Ad-hoc-Befehls}{15}{lstlisting.7.1}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {7.2}Beispiel eines Ansible-Ad-hoc-Befehls}{15}{lstlisting.7.2}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {7.3}Inventory}{15}{lstlisting.7.3}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {7.4}R\IeC {\"u}ckgabewerte von chronyd}{16}{lstlisting.7.4}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {7.5}\IeC {\"U}perpr\IeC {\"u}fung des Status von chronyd}{16}{lstlisting.7.5}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {7.6}Start von chronyd}{17}{lstlisting.7.6}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {7.7}Erneute \IeC {\"U}perpr\IeC {\"u}fung des Status von chronyd}{17}{lstlisting.7.7}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {7.8}Abfrage mit rpm -qi}{17}{lstlisting.7.8}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {7.9}Abfrage der Version von tzdata mit Ansible}{17}{lstlisting.7.9}
\defcounter {refsection}{0}\relax 
\addvspace {10\p@ }
\defcounter {refsection}{0}\relax 
\addvspace {10\p@ }
